# Solace Project
## Basic Walkthrough

[Walkthrough](https://youtu.be/oMMe6TN9_VQ)

## Screenshots
![screen\s](images/screens.png)

## Software needed 

**Internet Browser**- to watch video
**API 28+
**Android Studio**- to open project

## Special Instructions

### Login/Registration
For testing the login and registration work so you can register your own users.

### Chat
**To test the chat feature you are going to have to sign in with two devices as different users to simulate the listener and search functions**
 
### IOS Picture
**taking picture must be done on a actual device because custom cameras cant be opened on simulator**
